# ci-cd-workshop

[![pipeline status](https://gitlab.com/fatihacet/ci-cd-live/badges/master/pipeline.svg)](https://gitlab.com/fatihacet/ci-cd-live/commits/master) [![coverage report](https://gitlab.com/fatihacet/ci-cd-live/badges/master/coverage.svg)](https://gitlab.com/fatihacet/ci-cd-live/commits/master)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
